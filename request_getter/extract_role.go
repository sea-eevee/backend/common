package request_getter

import (
	"gitlab.com/sea-eevee/backend/common/tokenizer"
	"net/http"
)

func ExtractRole(r *http.Request) (string, error) {
	roles := []string{"admin", "customer", "merchant"}

	role := r.Header.Get(tokenizer.KeyHeaderRole)
	for _, ctr := range roles {
		if role == ctr {
			return role, nil
		}
	}

	return role, nil
}
