package request_getter

import (
	"gitlab.com/sea-eevee/backend/common/errs"
	"gitlab.com/sea-eevee/backend/common/tokenizer"
	"net/http"
	"strconv"
)

func ExtractUserID(r *http.Request) (uint64, error) {
	idStr := r.Header.Get(tokenizer.KeyHeaderUserID)
	if idStr == "" {
		return 0, errs.ErrNoUserID
	}

	id, err := strconv.ParseUint(idStr, 10, 64)
	if err != nil {
		return 0, err
	}

	return id, nil
}
