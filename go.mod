module gitlab.com/sea-eevee/backend/common

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/lib/pq v1.8.0
	github.com/spf13/viper v1.7.1
	gitlab.com/sea-eevee/marketplace-be v0.0.0-20200901145654-56ab5fd40841
)
