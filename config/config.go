package config

import (
	"fmt"
	"github.com/spf13/viper"
)

type Config struct {
	// DB
	DBHost string `mapstructure:"db_host"`
	DBPort int    `mapstructure:"db_port"`
	DBUser string `mapstructure:"db_user"`
	DBPass string `mapstructure:"db_password"`
	DBName string `mapstructure:"db_name"`

	// Token
	Secret            string `mapstructure:"secret"`
	JwtAccessExpires  int    `mapstructure:"jwt_at_expire"`
	JwtRefreshExpires int    `mapstructure:"jwt_rt_expire"`
}

func InitAppConfig(currPath string) *Config {
	viper.SetConfigName("config")
	viper.SetConfigType("env")
	viper.AddConfigPath(currPath)
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	config := new(Config)
	if err := viper.Unmarshal(config); err != nil {
		panic(fmt.Errorf("failed to parse config file: %w", err))
	}

	return config
}
