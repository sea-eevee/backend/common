package tokenizer

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/sea-eevee/backend/common/errs"
	"gitlab.com/sea-eevee/backend/common/responder"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func extractToken(bearToken string) string {
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}

func (t *tokenizer) MiddlewareAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		clientToken := extractToken(r.Header.Get("Authorization"))
		accessDetail, err := t.decodeTokenDetails(clientToken)
		if err != nil {
			log.Println(err)
			responder.ResponseError(w, err)
			return
		}

		r.Header.Set(KeyHeaderRole, accessDetail.Role)
		r.Header.Set(KeyHeaderUserID, strconv.FormatUint(accessDetail.UserId, 10))
		next.ServeHTTP(w, r)
	})
}

func (t *tokenizer) decodeTokenDetails(rawToken string) (*AccessDetails, error) {
	tokenParsed, err := jwt.Parse(rawToken, checkConformHMAC(t.secret))
	if err != nil {
		return nil, err
	}
	claims, ok := tokenParsed.Claims.(jwt.MapClaims)
	if !ok || !tokenParsed.Valid {
		return nil, err
	}
	role, ok := claims["role"].(string) //convert the interface to string
	if !ok {
		return nil, errs.ErrAuth
	}
	userId, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["user_id"]), 10, 64)
	if err != nil {
		return nil, errs.ErrAuth
	}
	return &AccessDetails{
		UserId: userId,
		Role:   role,
	}, nil
}

func checkConformHMAC(secret string) func(token *jwt.Token) (interface{}, error) {
	return func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(secret), nil
	}
}
