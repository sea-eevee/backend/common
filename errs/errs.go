package errs

import "errors"

var (
	ErrBadRequest = errors.New("err bad request")
	ErrAuth       = errors.New("err auth")
	ErrInternal   = errors.New("err internal")
	ErrNoUserID   = errors.New("err no user id")
)
