package store

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

type DBConn struct {
	DBHost string
	DBPort int
	DBUser string
	DBPass string
	DBName string
}

func NewPostgres(param DBConn) (*sql.DB, error) {
	dbInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		param.DBHost, param.DBPort, param.DBUser, param.DBPass, param.DBName)
	log.Println(dbInfo)
	db, err := sql.Open("postgres", dbInfo)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	log.Print("database connection success")
	return db, nil
}
